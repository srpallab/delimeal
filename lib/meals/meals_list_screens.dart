import './meals_model.dart';
import './meals_list_widgets.dart';
import 'package:flutter/material.dart';

class MealsListScreen extends StatefulWidget {
  static const routeName = '/meals-list';

  final List<Meal> availMeals;

  MealsListScreen(this.availMeals);

  @override
  MealsListScreenState createState() => MealsListScreenState();
}

class MealsListScreenState extends State<MealsListScreen> {
  var _isFirstTime;
  Map<String, String> args;
  List<Meal> meals;

  @override
  void initState() {
    _isFirstTime = true;
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isFirstTime == true) {
      args = ModalRoute.of(context).settings.arguments;
      meals = widget.availMeals.where((meal) => meal.categories.contains(args['id'])).toList();
      _isFirstTime = false;
    }
    super.didChangeDependencies();
  }

  //void _removeMeal(String mealId) {
    //setState(() {
      //meals.removeWhere((meal) => meal.id == mealId);
    //});
  //}

  @override
  Widget build(BuildContext context) {
    // print(meals[0].title);
    return Scaffold(
      appBar: AppBar(
        title: Text(args['title']),
      ),
      body: ListView.builder(
        itemCount: meals.length,
        itemBuilder: (ctx, index) {
          return MealsListWidgets(
            id: meals[index].id,
            title: meals[index].title,
            imageUrl: meals[index].imageUrl,
            duration: meals[index].duration,
            complexity: meals[index].complexity,
            affordability: meals[index].affordability,
            // removeItem: _removeMeal,
          );
        },
      ),
    );
  }
}
