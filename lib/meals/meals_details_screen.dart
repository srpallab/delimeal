import 'package:flutter/material.dart';
import './DUMMY_DATA_MEALS.dart';
import './meals_model.dart';
import './build_selection_widget.dart';
import './build_container_widget.dart';

class MealsDetailsScreen extends StatelessWidget {
  static const routeName = '/meal-details';
  final Function isFavorite;
  final Function toggleFavorite;

  MealsDetailsScreen(this.isFavorite, this.toggleFavorite);

  @override
  Widget build(BuildContext context) {
    final String mealId = ModalRoute.of(context).settings.arguments;
    final Meal meal = DUMMY_MEALS.firstWhere((meal) => meal.id == mealId);

    return Scaffold(
      appBar: AppBar(
        title: Text(meal.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 300,
              width: double.infinity,
              child: Image.network(
                meal.imageUrl,
                fit: BoxFit.cover,
              ),
            ),
            buildSelectionWidget(context, "Ingredients"),
            buildContainerWidget(
              context,
              ListView.builder(
                itemCount: meal.ingredients.length,
                itemBuilder: (ctx, index) {
                  return Card(
                      color: Theme.of(context).accentColor,
                      child: Padding(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          meal.ingredients[index],
                        ),
                      ));
                },
              ),
            ),
            buildSelectionWidget(context, "Steps"),
            buildContainerWidget(
              context,
              ListView.builder(
                itemCount: meal.steps.length,
                itemBuilder: (ctx, index) => ListTile(
                  leading: CircleAvatar(
                    child: Text('# ${index + 1}'),
                  ),
                  title: Text(meal.steps[index]),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
            onPressed: () => toggleFavorite(mealId),
            child: isFavorite(mealId) ? Icon(Icons.star) : Icon(Icons.star_border),
          ),
    );
  }
}


//Row(
//  children: [
//    FloatingActionButton(
//      onPressed: () => Navigator.pop(context, mealId),
//      child: Icon(Icons.delete),
//    ),
//  ],
//),
