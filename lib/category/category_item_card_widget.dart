import 'package:flutter/material.dart';
import '../meals/meals_list_screens.dart';

class CategoryItemCardWidget extends StatelessWidget {
  final String id;
  final String title;
  final Color color;

  CategoryItemCardWidget(this.id, this.title, this.color);

  void _gotoMealList(BuildContext ctx) {
    Navigator.of(ctx).pushNamed(
      MealsListScreen.routeName,
      arguments: {
        'id': id,
        'title': title,
      });
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => _gotoMealList(context),
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(15),
      child: Container(
        padding: EdgeInsets.all(20),
        child: Text(title, style: Theme.of(context).textTheme.bodyText1),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [color.withOpacity(0.7), color],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
          borderRadius: BorderRadius.circular(15),
        ),
      ),
    );
  }
}
