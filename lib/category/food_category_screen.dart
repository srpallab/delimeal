import 'package:flutter/material.dart';
import './dummy_data.dart';
import './category_item_card_widget.dart';

class FoodCategoryScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  GridView(
        padding: EdgeInsets.all(15),
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 200,
          childAspectRatio: 3 / 2,
          crossAxisSpacing: 20,
          mainAxisSpacing: 20,
        ),
        children: DUMMY_CATEGORIES.map((catData) => CategoryItemCardWidget(
            catData.id,
            catData.title,
            catData.color
        )).toList(),
      );
  }
}
