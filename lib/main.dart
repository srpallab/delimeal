import 'package:flutter/material.dart';

import './meals/DUMMY_DATA_MEALS.dart';
import './tabs/tabs_screen.dart';
import './meals/meals_model.dart';
import './meals/meals_list_screens.dart';
import './meals/meals_details_screen.dart';
import './tabs/settings_screen.dart';

void main() {
  runApp(DeliFood());
}

class DeliFood extends StatefulWidget {
  @override
  DeliFoodState createState() => DeliFoodState();
}

class DeliFoodState extends State<DeliFood> {
  Map<String, bool> _filters = {
    "gluten": false,
    "lactos": false,
    "vegan": false,
    "veg": false
  };

  List<Meal> availableMeals = DUMMY_MEALS;
  List<Meal> _favoriteMeals = [];

  void _filterMeals(Map<String, bool> setFilters) {
    setState(() {
      _filters = setFilters;
      availableMeals = availableMeals.where((meal) {
        if (_filters['gluten'] && !meal.isGlutenFree) {
          return false;
        }
        if (_filters['lactos'] && !meal.isLactoseFree) {
          return false;
        }
        if (_filters['vegan'] && !meal.isVegan) {
          return false;
        }
        if (_filters['veg'] && !meal.isVegetarian) {
          return false;
        }
        return true;
      }).toList();
    });
  }

  void _toggleFavorite(String id) {
    final existingIndex = _favoriteMeals.indexWhere((meal) => meal.id == id);
    if (existingIndex >= 0) {
      setState(() {
        _favoriteMeals.removeAt(existingIndex);
      });
    } else {
      setState(() {
        _favoriteMeals.add(
          DUMMY_MEALS.firstWhere((meal) => meal.id == id),
        );
      });
    }
  }

  bool _isMealFavorite(String id) {
    return _favoriteMeals.any((meal) => meal.id == id);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DeliMeal',
      theme: ThemeData(
        primaryColor: Colors.pink,
        accentColor: Colors.amber,
        canvasColor: Color.fromRGBO(255, 254, 229, 1),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
              headline1: TextStyle(color: Color.fromRGBO(20, 51, 50, 1)),
              headline2: TextStyle(color: Color.fromRGBO(20, 51, 50, 1)),
              bodyText1: TextStyle(fontFamily: 'RobotoCondensed', fontSize: 20),
            ),
      ),
      home: TabsScreen(_favoriteMeals),
      routes: {
        MealsListScreen.routeName: (ctx) => MealsListScreen(availableMeals),
        MealsDetailsScreen.routeName: (ctx) => MealsDetailsScreen(
          _isMealFavorite, _toggleFavorite
        ),
        SettingsScreen.routeName: (ctx) =>
            SettingsScreen(_filters, _filterMeals),
      },
    );
  }
}
