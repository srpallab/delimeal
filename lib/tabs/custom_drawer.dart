import 'package:flutter/material.dart';
import './settings_screen.dart';

class CustomDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, Function navPageLink) {
    return ListTile(
      onTap: navPageLink,
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: "RobotoCondensed",
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Container(
            color: Theme.of(context).accentColor,
            height: 150,
            width: double.infinity,
            alignment: Alignment.center,
            child: Text(
              "Cooking UP",
              style: TextStyle(
                fontWeight: FontWeight.w900,
                fontSize: 30,
                color: Theme.of(context).primaryColor,
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          buildListTile("Meals", Icons.restaurant, () {
            Navigator.pushReplacementNamed(context, '/');
          }),
          buildListTile("Filters", Icons.settings, () {
            Navigator.pushReplacementNamed(context, SettingsScreen.routeName);
          }),
        ],
      ),
    );
  }
}
