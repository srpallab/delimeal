import './custom_drawer.dart';
import 'package:flutter/material.dart';

class SettingsScreen extends StatefulWidget {
  static const routeName = '/settings';

  final Map<String, bool> filters;
  final Function avalMeals;

  SettingsScreen(this.filters, this.avalMeals);

  @override
  SettingsScreenState createState() => SettingsScreenState();
}

class SettingsScreenState extends State<SettingsScreen> {
  var _glutenFree = false;
  var _veg = false;
  var _vegan = false;
  var _lactosFree = false;

  @override
  void initState() {
    _glutenFree = widget.filters['gluten'];
    _lactosFree = widget.filters['lactos'];
    _vegan = widget.filters['vegan'];
    _veg = widget.filters['veg'];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Your Filters"),
        actions: [
          InkWell(
            onTap: () {
              widget.avalMeals(
                {
                  "gluten": _glutenFree,
                  "lactos": _lactosFree,
                  "vegan": _vegan,
                  "veg": _veg
                },
              );
            },
            child: Icon(Icons.save),
          ),
          SizedBox(
            width: 20,
          ),
        ],
      ),
      drawer: CustomDrawer(),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: Text(
              "Adjust your meal selections.",
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          Expanded(
            child: ListView(
              children: [
                SwitchListTile(
                  title: Text("Lactos Free"),
                  subtitle: Text("Display only lactos free meals."),
                  value: _lactosFree,
                  onChanged: (newValue) {
                    setState(() {
                      _lactosFree = newValue;
                    });
                  },
                ),
                SwitchListTile(
                  title: Text("Gluten Free"),
                  subtitle: Text("Display only gluten free meals."),
                  value: _glutenFree,
                  onChanged: (newValue) {
                    setState(() {
                      _glutenFree = newValue;
                    });
                  },
                ),
                SwitchListTile(
                  title: Text("Vegan"),
                  subtitle: Text("Display only vegan meals."),
                  value: _vegan,
                  onChanged: (newValue) {
                    setState(() {
                      _vegan = newValue;
                    });
                  },
                ),
                SwitchListTile(
                  title: Text("Vegitarian"),
                  subtitle: Text("Display only vagetarina meals."),
                  value: _veg,
                  onChanged: (newValue) {
                    setState(() {
                      _veg = newValue;
                    });
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
